value = 0

File.open("day_1_input.txt", "r") do |f|
  f.each_line do |line|
    value = value + line.to_i
  end
end

puts value