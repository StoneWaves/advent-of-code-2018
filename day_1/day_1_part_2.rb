output    = []
value     = 0
frequency = false

until frequency
  File.open("day_1_input.txt", "r") do |f|
    f.each_line do |line|
      value = value + line.to_i

      if output.include?(value)
        puts value
        frequency = true
        abort("Value is #{value}")
      else
        output.push(value)
        puts value
      end
    end
  end
end


